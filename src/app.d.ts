/// <reference types="@sveltejs/kit" />

declare namespace App {
	interface Locals {
		dbClient: typeof dbClient
	}

	interface Platform {
	}

	interface Session {
	}

	interface Stuff {
	}
}