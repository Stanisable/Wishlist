import 'dotenv/config'
import { Client } from 'pg'
import type { ResolveOpts } from '@sveltejs/kit/types/hooks'
import type { MaybePromise } from '@sveltejs/kit/types/helper'
import type { RequestEvent } from '@sveltejs/kit'

const dbClient = new Client()

dbClient.connect()
	.then(() => {
		console.log('🟢 Connection to database successfull')
	})
	.catch(error => console.error('❌ Connection to database failed', error.stack))

dbClient.on('error', error => {
	console.error('🔴 Error with database, action required', error.stack)
})


export function handle({ event, resolve }: {
	event: RequestEvent;
	resolve(event: RequestEvent, opts?: ResolveOpts): MaybePromise<Response>;
}): MaybePromise<Response> {
	event.locals.dbClient = dbClient
	return resolve(event)
}
