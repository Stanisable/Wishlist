import type { EndpointOutput, RequestEvent } from '@sveltejs/kit'

type postWishListBody = { deletionDate: Date, name: string }

export const post = async ({
														 request,
														 params,
														 locals,
													 }: RequestEvent): Promise<EndpointOutput> => {
	const { listId } = params
	const { dbClient } = locals
	const { deletionDate, name } = await request.json() as postWishListBody

	const query = {
		text: 'INSERT INTO wishlist (id, deletionDate, name) VALUES ($1, $2, $3)',
		values: [listId, deletionDate, name],
	}

	await dbClient.query(query)

	return {
		status: 201,
	}
}

export const get = async ({ params, locals }: RequestEvent): Promise<EndpointOutput> => {
	const { listId } = params
	const { dbClient } = locals
	const query = {
		text: 'SELECT * FROM wishlist WHERE id = $1',
		values: [listId],
	}

	const wishListQuery = await dbClient.query(query)
	const wishList = wishListQuery.rows[0]

	return {
		status: 200,
		body: wishList,
	}

}