import preprocess from 'svelte-preprocess'
import adapter from '@sveltejs/adapter-node'
import tailwindcss from 'tailwindcss'

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter(),
		vite: {
			css: {
				postcss: {
					plugins: [
						tailwindcss,
					]
				}
			}
		}
	}
};

export default config;
